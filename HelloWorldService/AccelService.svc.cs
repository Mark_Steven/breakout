﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using BreakoutASP.Business;
using BreakoutASP.Models;

namespace HelloWorldService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class AccelService : IAccelService
    {
        public AccelSensorModel saveAccelSensorData(AccelSensorModel model)
        {
            model.sensorName = "Main";
            // Log the API call
            //		logger.info("Entering RestService.saveTemperatureSensorData()");
            //		logger.debug("Model: " + model.toString());

            // Send roll to a Move object
            MovePaddleModel currentMove = new MovePaddleModel(model.roll);

            // Send move to MoveBusiness
            MoveBusiness.addToQueue(currentMove);

            // Return OK Response
            //ResponseModel response = new ResponseModel(1, "OK");

            return model;
        }
    }
}
