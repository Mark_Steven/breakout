﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BreakoutASP.Models
{
    public class Brick
    {
        public Brick(int IndexX, int IndexY)
        {
            this.IndexX = IndexX;
            this.IndexY = IndexY;
            this.X = IndexX * 95 + 3;
            this.Y = IndexY * 45 + 3;
            Broken = false;
        }
        public int IndexX { get; set; }
        public int IndexY { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public bool Broken { get; set; }
    }
}