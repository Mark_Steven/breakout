﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BreakoutASP.Models
{
    public class ResponseDataModel : ResponseModel
    {
        public AccelSensorModel Data { get; set; }

        public ResponseDataModel() : base(0, "")
        {
            Data = new AccelSensorModel(0, 0, 1999);
        }

        public ResponseDataModel(int status, String message, AccelSensorModel data) : base(status, message)
        {
            Data = data;
        }
    }
}