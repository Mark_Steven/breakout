﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BreakoutASP.Models
{
    public class AccelSensorModel
    {
        public string sensorName { get; set; }
        public double pitch { get; set; }
        public double roll { get; set; }
        public double yaw { get; set; }

        public AccelSensorModel()
        {
            sensorName = "";
            pitch = 0;
            roll = 0;
            yaw = 0;
        }

        public AccelSensorModel(double pitch, double roll, double yaw)
        {
            this.sensorName = "Main";
            this.pitch = pitch;
            this.roll = roll;
            this.yaw = yaw;
        }
    }
}