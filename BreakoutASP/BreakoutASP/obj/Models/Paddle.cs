﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BreakoutASP.Model
{
    public class Paddle
    {
        public Paddle()
        {
            DX = 0;
            X = 200;
            Y = 550;
        }
        public int DX { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
    }
}
