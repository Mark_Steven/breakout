﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BreakoutASP.Models
{
    public class ResponseModel
    {
        public int Status { get; set; }
        private String Message { get; set; }

        public ResponseModel(int status, String message)
        {
            this.Status = status;
            this.Message = message;
        }
    }
}