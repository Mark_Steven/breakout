﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BreakoutASP.Models
{
    public class Ball
    {
        public Ball()
        {
            X = 460;
            Y = 450;
            DX = 1;
            DY = -1;
        }
        public int X { get; set; }
        public int Y { get; set; }
        public int DX { get; set; }
        public int DY { get; set; }
    }
}