﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BreakoutASP.Models
{
    public class Movement
    {
        public Movement(int X, int Y, int PrevX, int PrevY, string Index)
        {
            this.Index = Index;
            this.X = X;
            this.Y = Y;
            this.PrevX = PrevX;
            this.PrevY = PrevY;
        }
        public String Index { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int PrevX { get; set; }
        public int PrevY { get; set; }
    }
}