﻿using AzureBreakout.Helper;
using BreakoutASP.Business;
using BreakoutASP.Helper;
using BreakoutASP.Model;
using BreakoutASP.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BreakoutASP.Controllers
{
    public class GameController : Controller
    {
        // All game objects (Paddle, Ball, and Brick array)
        PaddleHelper paddleHelper;
        BallHelper ballHelper;
        CollisionHelper collisionHelper;

        int lastBallX, lastBallY, lastPaddleX = 0;

        Brick[] brickArr;
        // Movement list
        List<Movement> allMovements;
        AccelSensorModel recentMove;

        // Switch out for Injected Interface
        MoveBusiness business;

        // GET: Game and set main objects before storing them in the Seesion
        public ActionResult Index()
        {
            Trace.WriteLine("Output");
            Debug.WriteLine("Inside Index");
            business = new MoveBusiness();

            paddleHelper = new PaddleHelper();
            ballHelper = new BallHelper();
            collisionHelper = CollisionHelper.Instance;

            brickArr = new Brick[45];

            collisionHelper.brickArr = new Brick[45];

            for (var i = 0; i < 5; i++)
            {
                for (var j = 0; j < 9; j++)
                {
                    brickArr[i * 9 + j] = new Brick(j, i);
                    collisionHelper.brickArr[i * 9 + j] = new Brick(j, i);
                }
            }

            collisionHelper.ball = ballHelper.ball;

            Session["paddle"] = paddleHelper;
            Session["ball"] = ballHelper;
            Session["bricks"] = brickArr;
            return View("GameView");
        }

        // Mildly convoluted AJAX method that hits the REST service
        public JsonResult Step()
        {
            Debug.WriteLine("Inside Update");
            collisionHelper = CollisionHelper.Instance;
            // Start timer

            // Create an array of Movement objects to hold any changes
            // Index 0 holds the paddle
            // Index 1 holds the ball
            // Indexes 2-50 hold bricks
            allMovements = new List<Movement>();
            
            // Reset session variables
            paddleHelper = (PaddleHelper)Session["paddle"];
            ballHelper = (BallHelper)Session["ball"];
            brickArr = (Brick[])Session["bricks"];
            
            collisionHelper.Player = paddleHelper.GetPaddle();
            collisionHelper.ball = ballHelper.ball;

            // Hit the array holding all of the movements and take the oldest movement. This should make the array empty
            recentMove = MoveBusiness.GetFirstInQueue();

            // Update the position of the paddle
            if (recentMove.pitch > -1)
            {
                lastPaddleX = paddleHelper.GetX();
                paddleHelper.Direction(recentMove.pitch);
                paddleHelper.Move();
                allMovements.Add(new Movement(paddleHelper.GetX(), paddleHelper.GetY(), lastPaddleX, 0, "paddle"));
            }

            // Update the position of the ball
            lastBallX = ballHelper.GetX();
            lastBallY = ballHelper.GetY();

            // Returns the location of a brick that was broken or -1 if none were
            int brickBroken = collisionHelper.BrickCollision();

            if (brickBroken != -1)
            {
                brickArr[brickBroken] = collisionHelper.brickArr[brickBroken];
                ballHelper.ball = collisionHelper.ball;
                // Update bricks if one is broken
                allMovements.Add(new Movement(brickArr[brickBroken].X, brickArr[brickBroken].Y, 0, 0, "brick" + 
                    brickArr[brickBroken].IndexX + ", " + brickArr[brickBroken].IndexY));
                ballHelper.ball = collisionHelper.ball;
            }
            else if(collisionHelper.PlayerCollision())
            {
                ballHelper.ball = collisionHelper.ball;
            }
            ballHelper.Move();

            allMovements.Add(new Movement(ballHelper.GetX(), ballHelper.GetY(), lastBallX, lastBallY, "ball"));


            Debug.WriteLine("Outputting json: " + Json(allMovements));
            //Stop timer, should be less then the time between "frames"
            // Send JSON string to method
            return Json(allMovements, JsonRequestBehavior.AllowGet);
        }
    }
}