﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BreakoutASP.Models;
using BreakoutASP.Model;
using BreakoutASP.Controllers;

namespace AzureBreakout.Helper
{
    public sealed class CollisionHelper
    {
        public Ball ball { get; set; }
        public Brick[] brickArr { get; set; }
        public Paddle Player { get; set; }

        private static readonly CollisionHelper instance = new CollisionHelper();

        private CollisionHelper()
        {
            ball = new Ball();
            brickArr = new Brick[1];
            Player = new Paddle();
        }

        public static CollisionHelper Instance
        {
            get
            {
                return instance;
            }
        }

        // Returns the location of a brick that was broken or -1 if none were
        public int BrickCollision()
        {
            for (int i = 0; i < brickArr.Length; i++)
            {
                Brick block = brickArr[i];
                if (!block.Broken)
                {

                    //Moving in a counterclockwise angle around the brick.
                    //Brick dimentions are 95 X 45

                    //Condition 4: ball is moving up to the right, hits the bottom of the brick. DX+ DY-
                    // [X][X]
                    if (ball.Y <= block.Y + 45 && ball.Y >= block.Y + 35 && ball.X >= block.X - 5 && ball.X < block.X + 90 && ball.DX >= 1 && ball.DY == -1)
                    {
                        //Removed for debuggin purposes.
                        ball.DY = 1;
                        block.Broken = true;
                        return i;
                    }
                    //Condition 5: ball is moving up to the left, hits the bottom of the brick. DX- DY-
                    // [X][???]
                    //        ball.Y <= block.Y + 50 && ball.Y >= block.Y + 40 && block.X >= ball.X - 20 && block.X + 90 < ball.X && ball.DX <= -1 && ball.DY == -1
                    else if (ball.Y <= block.Y + 45 && ball.Y >= block.Y + 35 && ball.X >= block.X - 5 && ball.X < block.X + 90 && ball.DX >= -1 && ball.DY == -1)
                    {
                        //ball.DY = 1;
                        block.Broken = true;
                        return i;
                    }
                    // Condition 1: Ball is moving down to the right, hits the top of the brick. DX+ DY+
                    // [X][X] Needs refining. 
                    else if (ball.Y <= block.Y && ball.Y + 50 >= block.Y - 10 && ball.X >= block.X - 5 && ball.X < block.X + 90 && ball.DX >= 1 && ball.DY == 1)
                    {
                        //ball.DY = -1;
                        block.Broken = true;
                        return i;
                    }
                    //Condition 2: ball is moving down to the right, hits the left side of the brick. DX+ DY+
                    // [X][X]
                    else if (ball.X + 40 >= block.X - 10 && ball.X + 10 <= block.X && ball.Y >= block.Y - 20 && ball.Y < block.Y + 50 &&
                        ball.DX == 1 && ball.DY == 1)
                    {
                        ball.DX = -1;
                        block.Broken = true;
                        return i;
                    }
                    //Condition 3: ball is moving up to the right, hits the side of the brick. DX+ DY-
                    // [X][X]
                    else if (ball.X + 40 >= block.X - 10 && ball.X + 40 <= block.X && ball.Y + 20 >= block.Y - 10 && ball.Y < block.Y + 50 &&
                        ball.DX == 1 && ball.DY == -1)
                    {
                        //ball.DX = -1;
                        block.Broken = true;
                        return i;
                    }
                    //Condition 6: ball is moving up to the left, hits the right side of the brick. DX- DY-
                    // [X] Works conditionally, Brick does not break.
                    // Y WORKS   
                    //Brick dimentions are 95 X 45
                    // where ball hits is (25) and (3)
                    else if //(ball.X <= block.X + 100 && ball.Y -20 <= block.Y +45 && ball.Y -20 < block.Y  && ball.DX == -1 && ball.DY == -1)
                    (ball.Y + 20 >= block.Y && ball.Y + 20 < block.Y + 30 && ball.X <= block.X + 85 &&
                        ball.DX == -1 && ball.DY == -1)
                    {
                        block.Broken = true;
                        //ball.DX = 1;
                        
                        return i;
                    }
                     //Condition 7: ball is moving down to the left, hits the side of the brick. DX- DY+
                    // [X][X]
                    else if (ball.Y + 40 >= block.Y && ball.Y <= block.Y - 10 && ball.X >= block.X + 90 && ball.X <= block.X + 100 &&
                        ball.DX == -1 && ball.DY == 1)
                    {
                       //ball.DX = 1;
                       block.Broken = true;
                        return i;
                    }
                    //Condition 8: ball is moving down to the left, hits the top of the brick. DX-, DY+
                    //[X][X]
                    //(ball.Y <= block.Y + 45 && ball.Y >= block.Y + 35 && ball.X >= block.X - 5 && ball.X < block.X + 90 && ball.DX >= -1 && ball.DY == -1)
                    else if (ball.Y >= block.Y  && ball.Y >= block.Y + 35 && ball.X >= block.X -5 && ball.X < block.X + 90 && ball.DX == -1 && ball.DY == 1)
                    {
                        //ball.DY = -1;
                       block.Broken = true;
                        return i;
                    }
                }
            }
            return -1;
        }
        // Returns whether the ball collided with the paddle or not
        public bool PlayerCollision()
        {
            //Due to how it would be impossible for the ball to hit the Player at an upward angle without
            //causing a gameover, any conditions where DY is negitive is not accounted for.
            //Player dimentions: 200 x  550

            // Condition 1: Ball is moving down to the right, hits the top of the brick. DX+ DY+
            // Tested and working
            // [X]
            if (ball.Y + 35 >= Player.Y && ball.Y + 45 < Player.Y && Player.X <= ball.X && Player.X + 120 > ball.X && ball.DX == -1)
            {
                ball.DY = -1;
                return true;
            }
            //vCondition 2: ball is moving down to the right, hits the side of the brick. DX+ DY+
            // [X]
            if (ball.X + 435 >= Player.X && ball.X + 40 <= Player.X && Player.Y <= ball.Y && ball.Y <= Player.Y + 100 && ball.DY >= 1)
            {
                ball.DX = -1;
                ball.DY = 1;
                return true;
            }

            // Condition 3: ball is moving down to the left, hits the side of the brick. DX- DY+
            // [ ]
            if (ball.X >= Player.X + 545 && ball.X < Player.X + 555 && Player.Y <= ball.Y + 20 && ball.Y + 20 <= Player.Y + 200 && ball.DX == -1 && ball.DY == 1)
            {
                ball.DY = -1;
                return true;
            }
            //Condition 4: ball is moving down to the left, hits the top of the brick. DX-, DY+
            //[ ]
            if (ball.Y + 35 >= Player.Y && ball.Y + 45 < Player.Y && Player.X <= ball.X && Player.X + 200 >= ball.X && ball.DX == -1 && ball.DY == 1)
            {
                ball.DY = -1;
                return true;
            }

            return false;
        }
    }
}