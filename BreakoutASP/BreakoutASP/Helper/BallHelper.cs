﻿using BreakoutASP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BreakoutASP.Helper
{
    public class BallHelper
    {
        public Ball ball { get; set; }
        private bool ballMove;
        // Number of pixels moved per frame
        private int speed;

        public BallHelper()
        {
            this.ball = new Ball();
            speed = 10;
            ballMove = true;
        }

        public void Move()
        {

            //Collssion detection for the walls goes in here.
            // PSUDO CODE:
            // if (ball.x == (625 - wallX) then ball.DX = (ball.DX *-1) )

            ballMove = true;
            if (ball.X >= 825 || ball.X <= 5)
            {
                ball.DX *= -1;
            }

            else if (ball.Y >= 955)
            {
                ball.DY = 0;
                ballMove = false;
            }
            else if (ball.Y <= 5)
            {
                //ball.X = ball.X + (ball.DX * speed);
                ball.DY *= -1;
            }
            if (ballMove)
            //else if (ball.Y < 955 && ball.Y > 5)
            {
                ball.Y = ball.Y + (ball.DY * speed);
                ball.X = ball.X + (ball.DX * speed);

            }
        }


        public int GetX()
        {
            return ball.X;
        }

        public int GetY()
        {
            return ball.Y;
        }
    }
}