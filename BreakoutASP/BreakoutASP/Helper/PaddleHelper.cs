﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BreakoutASP.Model;
namespace BreakoutASP.Helper
{
    public class PaddleHelper
    {
        private Paddle paddle;
        private bool PaddleMove;
        private int Speed;

        public PaddleHelper()
        {
            this.paddle = new Paddle();
            PaddleMove = true;
            // Speed will be determined by the pitch of the pi.
            Speed = 30;
        }

        public void Direction(double input)
        {
            // Modify DX
            if (input >= 35 && input < 180)
            {
                paddle.DX = 1;
            }
            else if (input >= 180 && input < 324)
            {
                paddle.DX = -1;
            }
            else
            {
                paddle.DX = 0;
            }
        }

        public void Move()
        {
            // === NOTES ===
            // DX + moves right
            // DX - moves left
            // 0 = left side of the screen.
            // Canvas size = width="1000" height="650
            // Paddle size = 47 total (paddle width = 40 and line width = 7)
            // Wall size = 3.
            // Therefor the range of movement for the paddle should be from 50 to 950.
            PaddleMove = true;
            //120
            if (paddle.X >= 870 && paddle.DX == 1)
            {
                //This will stop the paddle from moving any further right off screen.
                PaddleMove = false;
            }
            else if (paddle.X <= 10 && paddle.DX == -1)
            {
                //This will stop the paddle from moving any further left off screen.
                PaddleMove = false;
            }
            if (PaddleMove)
            {
                paddle.X = paddle.X + (paddle.DX * Speed);
            }
            //paddle.X <= 45
        }
        public int GetX()
        {
            return paddle.X;
        }

        public int GetY()
        {
            return paddle.Y;
        }

        public Paddle GetPaddle()
        {
            return paddle;
        }
    }
}