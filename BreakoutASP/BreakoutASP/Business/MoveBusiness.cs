﻿using BreakoutASP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BreakoutASP.Business
{
    public class MoveBusiness
    {
        public static List<AccelSensorModel> paddleQueue { get; set; }

        public MoveBusiness()
        {
            paddleQueue = new List<AccelSensorModel>();
            paddleQueue.Add(new AccelSensorModel(250.0, 0, 0));
        }

        public static void addToQueue(AccelSensorModel move)
        {
            paddleQueue.Add(move);
        }

        public static AccelSensorModel GetFirstInQueue()
        {
            if (paddleQueue.Count > 0)
            {
                AccelSensorModel move = paddleQueue.ElementAt(0);
                paddleQueue.RemoveAt(0);
                return move;
            }
            return new AccelSensorModel();
        }
    }
}